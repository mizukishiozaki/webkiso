
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8";
    <title></title>
</head>
<body>
<?php
//変数に数値を代入します。
$count = 1;
$sum = 0;

//繰り返し処理を行います。
while($count <= 100){
    $sum += $count;
    
    if($sum > 1000){
        echo '1000を超えたのでcountは' .$count. 'で終了します<br>';
        break;
    }
    $count += 1;
}
echo 'sumは' .$sum;
?>
</body>
</html>
