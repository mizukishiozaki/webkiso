<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8";
    <title></title>
</head>
<body>
<?php
    $birth_year = 1997;
    $now_year = date('Y');
    
    echo '私は' . $birth_year . '年生まれです。今は' . ($now_year - $birth_year) . '歳です。<br>';

    $age = $now_year - $birth_year;
    echo "私は{$birth_year}年生まれです。今は" . $age . "歳です。<br>";
    
?>
</body>
</html>