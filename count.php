<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<?php
$names = ['浅野','伊藤','宇田','江本'];
echo count($names);
echo '<br>';

echo '<pre>';
var_dump($names);
echo '</pre>';
echo '<br>';

$names[] = '太田';
echo count($names);
echo '<br>';

echo '<pre>';
var_dump($names);
echo '<pre>';
echo '<br>';

echo $names [count($names)-1];
echo '<br>';
$empty = [];
echo count($empty);
echo '<br>';
$test = 0;

if(is_array($test)){
    echo count($test);
}else{
    echo '配列ではありません';
}

echo '<br>';

$test = [100];
if(is_array($test)){
    echo count($test);
}else{
    echo '配列ではありません';
}

echo '<br>';

$test = 100;
$count = (is_array ($test)) ?
        count($test) : '×';
echo $count;
?>
</body>
</html>