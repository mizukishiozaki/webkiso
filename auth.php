<?php
//ユーザーリスト
function get_users(){
    /*return = array(
        array('id' => 'akb','pass' =>'akb48'),
        array('id' => 'ske','pass' =>'ske48')
    );*/
    
    $data = file_get_contents('users.json');

    if($data === false){
        echo 'データが読み込めませんでした';
        return array();
    }else{
        $array = json_decode($data,true);
        return $array;
    }
}

function add_users($name,$pass){
    $users = get_users();
    $users[] = array('id' => $name,'pass'=>$pass);
    
    $json = json_encode($users,JSON_PRETTY_PRINT);
    file_put_contents('users.json',$json);
}

function password_check($name,$pass){
    $users = get_users();
    
    foreach($users as $user){
        if($user['id'] == $name &&
        $user['pass'] == $pass){
            //ユーザー名・パスワードが合っていた
        return true;
    }
}
//合っているものがなかった
return false;
}
/*
//ユーザー名とパスワードが一致するかの動作テスト
//3項演算子を使って上で作った関数をチェック
echo (password_check ('ske','ske48'))? 'OK':'NG';
echo '<br>';
echo(password_check ('ske','ske96'))? 'OK':'NG';
*/




