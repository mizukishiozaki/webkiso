<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8";
    <title></title>
</head>
<body>
<?php
function get_next_year(){
    $next_year = date('Y') +1;
    return $next_year;
}

$birth_year = 1997;
$age = get_next_year() - $birth_year;
echo "私は{$birth_year}年生まれです。" . "来年は" . $age . "歳です。";
?>
</body>
</html>
