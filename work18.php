<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<?php

$weeks = ['日曜','月曜','火曜','水曜','木曜','金曜','土曜'];

echo $weeks[0].'<br>';
echo $weeks[2].'<br>';
echo $weeks[4].'<br>';

for($i=0; $i<=3; $i++){
    echo $weeks[$i].'<br>';
}


$timestamp = mktime(0,0,0,6,28,17);
$week = date('w',$timestamp);

echo date('Y年m月j日は、',$timestamp).$weeks[$week].'日です'.'<br>';

var_dump($weeks);
?>
    
</body>
</html>
