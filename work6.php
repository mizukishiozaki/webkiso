<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8";
    <title></title>
</head>
<body>
<?php
function is_multiple3($value){
    //$valueが3の倍数かを判定
    //3で割った余りをとる
    $amari = $value % 3;
    //余りが0かを確認する
    if($amari == 0){
        //3の倍数
        echo "{$value}は3の倍数です";
    }else{
        //3の倍数ではない
        echo "{$value}は3の倍数ではありません";
    }
}

$value = 2;
is_multiple3($value);
$value = 3;
is_multiple3($value);
$value = 4;
is_multiple3($value);

is_multiple3(10);
is_multiple3(11);
is_multiple3(12);
?>
</body>
</html>

