<!DOCTYPE html>
<html land ="ja">
    <head>
        <meta charset="UTF-8">
        <title>変数と文字列の出力</title>
    </head>
    <body>
<?php
// 変数$nameに文字列を代入します。
$name = 'PHP逆引きレシピ';
//変数$Nameに文字列を代入します。$nameと$Nameは別の変数です。
$Name ='徹底入門';
        
//文字列と変数の値をecho文で表示します。
echo '書籍名: ' . $name . '<br>';
echo "書籍名: {$Name}\n<br>";
echo '書籍名: {$Name}\n<br>';
        
$format = '書籍名: %s、%s<br>';
echo sprintf($format,$name,$Name);
        
//複数行の文字列
$book = 'PHP逆引きレシピ';
$text = <<<EOL
ヒアドキュメントで変数に文章を代入します。<br>
書籍名 : $book<br>
EOL;
                
echo $text;
                
echo <<<END
echo　で直接文章を出力することも
できます。<br><br>
END;

echo <<<'END'
Nowdoc構文です。<br>
終端IDを、シングルクォートで囲んでいることに注意してください。<br>
以下に記述した変数は展開されません。<br>
書籍名 : $book<br>
END;
                
?>
    </body>
</html>

