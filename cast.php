<!DOCTYPE html>
<html land ="ja">
    <head>
        <meta charset="UTF-8">
        <title>変数と文字列の出力</title>
    </head>
    <body>
<?php
// 変数に整数を代入します。
$a = 123;
// 変数に文字列を代入します。
$b = '321';

echo '$a の型: ' . gettype($a) . '<br>';
echo '$b の型: ' . gettype($b) . '<br><br>';

// 関数を使い、型を変換します。
$a = straval($a);
$b = intval($b);

echo '$a の型: ' . gettype($a) . '<br>';
echo '$b の型: ' . gettype($b) . '<br><br>';

// 型キャストで型を変換します。
$a = (int) $a;
$b = (string) $b;

echo '$a の型: ' . gettype($a) . '<br>';
echo '$b の型: ' . gettype($b) . '<br><br>';

$c = 'abc';
$c = (int) $c;

echo '$cの型: ' . gittype($c) . '<br><br>';
echo '$cの中身: ' .$c;
?>
    </body>
</html>

