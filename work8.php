<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8";
    <title></title>
</head>
<body>
<?php
echo date('Y/m/d H:i:s',300000);
echo '<br>';
echo date('Y/m/d H:i:s',0);
echo '<br>';

function is_sunday($timestamp = null){
    //$timestampが渡されていなかったら
    //現在のタイムスタンプを代入
    if($timestamp === null){
        $timestamp = time();
    }
    echo date('Y/m/d',$timestamp).'は';
    $week = date('w',$timestamp);
    if($week == 0){
        echo "日曜日です<br>";
    }else{
        echo "日曜日ではありません<br>";
    }
}

$timestamp = time();
echo $timestamp . '<br>';
is_sunday($timestamp);

$timestamp2 = mktime(0,0,0,6,18,2017);
echo $timestamp2.'<br>';
is_sunday($timestamp2);
is_sunday();
?>
</body>
</html>
