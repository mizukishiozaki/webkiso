<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8";
    <title></title>
</head>
<body>
<?php
uruu(2017); //平年
uruu(2016); //うるう年
uruu(1900); //平年
uruu(2100); //平年
uruu(2000); //うるう年

function uruu($year){
    echo $year . '年 : ';
    
    //if(($year % 4 == 0 && $year % 100 != 0) || $year % 400 == 0){
        
    $a = ($year % 4 == 0); //TRUE or FALSE
    $b = ($year % 100 == 0); //TRUE or FALSE
    $c = ($year % 400 == 0); //TRUE or FALSE
    
    if(($a && !$b) || $c){
        echo 'うるう年';
    }else{
        echo '平年';
    }
    echo '<br>';
}
?>
</body>
</html>
