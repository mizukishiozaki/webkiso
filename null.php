<!DOCTYPE html>
<html land ="ja">
    <head>
        <meta charset="UTF-8">
        <title>null</title>
    </head>
    <body>
<?php
$a = null;
$b = '';

// どちらも値は何も表示されません
echo '$a: ' . $a . '<br>';
echo '$b: ' . $b . '<br>';

if($a == $b){
    echo '$a と $bは等しい(==)<br>';
}else{
    echo '$a とと $bは異なる(==)<br>';
}

if($a === $b){
    echo '$a と $b は等しい(===)<br>';
}else{
    echo '$a と $b は異なる(===)<br>';
}

if(is_null($a)){
    echo 'nullです' . '<br>';
}else{
    echo 'nullではない' . '<br>';
}

if(inset($a)){
    echo '定義されています' . '<br>';
}else{
    echo '定義されていません'. '<br>';
}

?>

